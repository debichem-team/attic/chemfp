[console_scripts]
simsearch = chemfp.commandline.simsearch:run
sdf2fps = chemfp.commandline.sdf2fps:run
rdkit2fps = chemfp.commandline.rdkit2fps:run
ob2fps = chemfp.commandline.ob2fps:run
fpcat = chemfp.commandline.fpcat:run
oe2fps = chemfp.commandline.oe2fps:run

